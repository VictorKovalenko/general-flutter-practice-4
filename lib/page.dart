import 'package:flutter/material.dart';
import 'package:general_flutter_prictece4/button.dart';
import 'package:general_flutter_prictece4/main.dart';

class layoutPage extends StatelessWidget {
  String titleText;

layoutPage(this.titleText);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(titleText),),
      drawer: Drawer(
        child: ListView(
          children: [
            SizedBox(height: 50),
            MyButton(MyHomePage(),'Page one'),
            SizedBox(height: 50),
            MyButton(PageTwo(),'Page Two'),
            SizedBox(height: 50),
            MyButton(PageThree(),'Page Three'),
            SizedBox(height: 50),
            MyButton(PageFour(),'Page Four'),
          ],
        ),
      ),
    );
  }
}

class PageTwo extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
   return layoutPage('Page Two');
  }
}

class PageThree extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return layoutPage('Page Three');
  }
}
class PageFour extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return layoutPage('Page Four');
  }
}
